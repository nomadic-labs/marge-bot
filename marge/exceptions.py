class ApiError(Exception):
    @property
    def error_message(self):
        args = self.args
        if len(args) < 2:
            return None

        arg = args[1]
        if isinstance(arg, dict):
            return arg.get("message")
        return arg


class BadRequest(ApiError):
    pass


class Unauthorized(ApiError):
    pass


class Forbidden(ApiError):
    pass


class NotFound(ApiError):
    pass


class MethodNotAllowed(ApiError):
    pass


class NotAcceptable(ApiError):
    pass


class Conflict(ApiError):
    pass


class Unprocessable(ApiError):
    pass


class InternalServerError(ApiError):
    pass


class UnexpectedError(ApiError):
    pass


class TooManyRequests(ApiError):
    pass


class BadGateway(ApiError):
    pass


class ServiceUnavailable(ApiError):
    pass


class GatewayTimeout(ApiError):
    pass
