from typing import Optional, Dict, Union
from . import gitlab

GET, POST = gitlab.GET, gitlab.POST


class GitlabJob(gitlab.Resource):
    def __init__(self, api, info, project_id, pipeline_id):
        info["project_id"] = project_id
        info["pipeline_id"] = pipeline_id
        super().__init__(api, info)

    @classmethod
    def jobs_of_pipeline(
        cls, project_id, pipeline_id, api, scope: Optional[str] = None
    ):
        params: Dict[str, Union[bool, str]] = {
            "include_retried": False,
        }
        if scope is not None:
            params["scope"] = scope
        jobs_info = api.call(
            GET(
                f"/projects/{project_id}/pipelines/{pipeline_id}/jobs",
                params,
            )
        )

        return [cls(api, job_info, project_id, pipeline_id) for job_info in jobs_info]

    @property
    def project_id(self):
        return self.info["project_id"]

    @property
    def pipeline_id(self):
        return self.info["pipeline_id"]

    @property
    def id(self):
        return self.info["id"]

    @property
    def status(self):
        return self.info["status"]

    @property
    def name(self):
        return self.info["name"]

    @property
    def allow_failure(self):
        return self.info["allow_failure"]

    def run(self):
        return self._api.call(
            POST(
                "/projects/{0.project_id}/jobs/{0.id}/play".format(self),
            )
        )
