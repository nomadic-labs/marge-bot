"""
Prometheus exporter
"""

import pkg_resources

from prometheus_client import (
    start_http_server,
    Info,
    Gauge,
    REGISTRY,
    PROCESS_COLLECTOR,
    PLATFORM_COLLECTOR,
)

# Custom metrics
margebot = Info("margebot", "Marge-bot info")
margebot_current_mr = Gauge(
    "margebot_current_mr",
    "Current Merge Request being processed",
    ["author_id", "iid", "title", "web_url"],
)
margebot_pending_mr_total = Gauge(
    "margebot_pending_mr_total",
    "Total pending Merge Requests",
    ["list"],
)


def start():
    # Unregister default metrics
    REGISTRY.unregister(PROCESS_COLLECTOR)
    REGISTRY.unregister(PLATFORM_COLLECTOR)
    # pylint: disable=W0212
    REGISTRY.unregister(REGISTRY._names_to_collectors["python_gc_objects_collected"])

    margebot.info({"version": pkg_resources.get_distribution("marge").version})
    reset()
    start_http_server(8000)


def reset():
    margebot_current_mr.clear()
    margebot_current_mr.labels(author_id="", iid="", title="", web_url="").set(0)
    margebot_pending_mr_total.clear()
    margebot_pending_mr_total.labels(list="").set(0)


def set_current_mr(merge_request):
    margebot_current_mr.clear()
    margebot_current_mr.labels(
        author_id=merge_request.author_id,
        iid=merge_request.iid,
        title=merge_request.title,
        web_url=merge_request.web_url,
    ).set(merge_request.iid)


def set_pending_mr(merge_requests):
    iids = ",".join(str(merge_request.iid) for merge_request in merge_requests)
    margebot_pending_mr_total.clear()
    margebot_pending_mr_total.labels(list=iids).set(len(merge_requests))
