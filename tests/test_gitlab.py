import os
import unittest
import pytest

from marge import gitlab, exceptions
from marge.gitlab import GET

HTTPBIN = "http://localhost:8080"


class TestVersion:
    def test_parse(self):
        assert gitlab.Version.parse("9.2.2-ee") == gitlab.Version(
            release=(9, 2, 2), edition="ee"
        )

    def test_parse_no_edition(self):
        assert gitlab.Version.parse("9.4.0") == gitlab.Version(
            release=(9, 4, 0), edition=None
        )


class TestApiCalls(unittest.TestCase):
    @pytest.mark.skipif(
        os.environ["TEST_ENV"] != "docker",
        reason="Testing the api call requires an instance of httpbin",
    )
    def test_success_immediately_no_response(self):
        api = gitlab.Api(HTTPBIN, "", append_api_version=False)
        self.assertTrue(api.call(GET("/status/202")))
        self.assertTrue(api.call(GET("/status/204")))
        self.assertFalse(api.call(GET("/status/304")))

    @pytest.mark.skipif(
        os.environ["TEST_ENV"] != "docker",
        reason="Testing the api call requires an instance of httpbin",
    )
    def test_failure_after_all_retries(self):
        api = gitlab.Api(HTTPBIN, "", append_api_version=False)

        with self.assertRaises(exceptions.Conflict):
            api.call(GET("/status/409"))

        with self.assertRaises(exceptions.TooManyRequests):
            api.call(GET("/status/429"))

        with self.assertRaises(exceptions.GatewayTimeout):
            api.call(GET("/status/504"))
