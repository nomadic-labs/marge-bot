from unittest.mock import Mock

from marge.gitlab import Api, GET
from marge.gitlab_job import GitlabJob


INFO = {
    "id": 47,
    "project_id": 1001,
    "pipeline_id": 50777,
    "status": "manual",
    "name": "trigger",
}


# pylint: disable=attribute-defined-outside-init
class TestGitlabJob:
    def setup_method(self, _method):
        self.api = Mock(Api)

    def test_gitlab_job_contain_manual_trigger(self):
        api = self.api
        payload = INFO
        api.call = Mock(return_value=[payload])

        result = GitlabJob.jobs_of_pipeline(
            project_id=INFO["project_id"],
            pipeline_id=INFO["pipeline_id"],
            api=api,
            scope="manual",
        )
        api.call.assert_called_once_with(
            GET(
                "/projects/{}/pipelines/{}/jobs".format(
                    INFO["project_id"], INFO["pipeline_id"]
                ),
                {"scope": "manual", "include_retried": False},
            )
        )
        assert [pl.info for pl in result] == [payload]

    def test_properties(self):
        gitlab_job = GitlabJob(
            api=self.api, project_id=1234, pipeline_id=56789, info=INFO
        )
        assert gitlab_job.id == 47
        assert gitlab_job.project_id == 1234
        assert gitlab_job.pipeline_id == 56789
        assert gitlab_job.status == "manual"
        assert gitlab_job.name == "trigger"
