let
  pkgs = import (builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/21.05.tar.gz";
  }) { };

in pkgs.stdenv.mkDerivation {
  name = "marge-bot-shell";
  buildInputs = [
    pkgs.git
    pkgs.gnumake
    pkgs.curl
    pkgs.poetry
    pkgs.python3
    pkgs.black
  ];

  shellHook = ''
    export HISTCONTROL=ignoreboth:erasedups
  '';
}
