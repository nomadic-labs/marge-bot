### `0.16.11`

#### Bugfixes

 - Fix error message for forbidden commit titles (!28)

 - Comments on MRs detected unmergeable while preprocessing the queue
   are formatted correctly (!28).

### `0.16.10`

#### New features

- Add new flag `--ci-on-early-failure`. When set to `reject`,
  marge-bot will consider a pipeline as failed and reject it as soon
  as it detects a failed job therein. When set to `reject-cancel`, the
  pipeline is canceled in addition to the rejection. (!24)

- Improve error message on `Unprocessable` merges, which may result
  from attempting to merge MRs with unmerged dependencies (!26).

### `0.16.5`
### `0.16.4`
### `0.16.3`

- Feat: Add launch final pipeline parameter.

### `0.16.2`

### `0.16.1`

- Fix : In case where MR is coming from NL fork, get jobs from pipeline faied

### `0.16.0`

- In case where pipeline contain a manual job with label 'trigger', margebot runs it

##
## Fork (Nomadic Labs)
##

### `0.15.0`

- Merge from upstream: [`0.10.1`](https://github.com/smarkets/marge-bot/releases/tag/0.10.1)

### `0.14.0`

- Fix approvals check for Gitlab CE [#13](https://gitlab.com/nomadic-labs/marge-bot/-/merge_requests/13)
- Prometheus exporter [#12](https://gitlab.com/nomadic-labs/marge-bot/-/merge_requests/12)
- Poetry `1.1.13`

### `0.13.1 to 0.13.3`

- Docker image improvements: multi-stage build, Debian 11, Python `3.9.12`, Git, SSH, GnuPG

### `0.13.0`

- Make it possible to specify several merge order keys, including label [#8](https://gitlab.com/nomadic-labs/marge-bot/-/merge_requests/8)
- Improve error comment on forbidden commit message patterns and do not check forbidden messages on squash [#9](https://gitlab.com/nomadic-labs/marge-bot/-/merge_requests/9)

### `0.12.0`

- Add option for retrying failed MRs [#6](https://gitlab.com/nomadic-labs/marge-bot/-/merge_requests/6)
- Add check for merge-ability of all MRs in the queue [#4](https://gitlab.com/nomadic-labs/marge-bot/-/merge_requests/4)
- Log the queue of MRs found [#3](https://gitlab.com/nomadic-labs/marge-bot/-/merge_requests/3)

### `0.11.0`

- Add support for commit message checks [#1](https://gitlab.com/nomadic-labs/marge-bot/-/merge_requests/1)

### `0.10.2`

- Merge upstream: [#318](https://github.com/smarkets/marge-bot/pull/318) [#321](https://github.com/smarkets/marge-bot/pull/321)

### `0.10.1`

- Fix: Retry API calls on network errors and server failures (5xx)
- Enhancement: Fix unit tests and linting issues
- Enhancement: Use poetry as the python build system

## Upstream (Smarkets)

### `0.10.1`

- Feature: Guarantee pipeline before merging [#340](https://github.com/smarkets/marge-bot/pull/340)

### `0.10.0`

- Feature: implement HTTPS support for cloning [#225](https://github.com/smarkets/marge-bot/pull/225) [#283](https://github.com/smarkets/marge-bot/pull/283)
- Feature: Make CI work with GitHub Actions [#308](https://github.com/smarkets/marge-bot/pull/308)
- Feature: Allow running marge-bot in CI pipelines or as a single CLI job [#289](https://github.com/smarkets/marge-bot/pull/289)
- Fix: Bump urllib3 from 1.26.4 to 1.26.5 [#310](https://github.com/smarkets/marge-bot/pull/310)
- Fix: Bump urllib3 from 1.26.3 to 1.26.4 [#306](https://github.com/smarkets/marge-bot/pull/306)
- Fix: Upgrade dependencies and fix lints and tests [#305](https://github.com/smarkets/marge-bot/pull/305)
- Fix: AccessLevel enum matches GitLab docs [#294](https://github.com/smarkets/marge-bot/pull/294)

### `0.9.5`

- Feature: Add new choice `assigned_at` to option `merge_order` [#268](https://github.com/smarkets/marge-bot/pull/268)
- Fix: Wait for merge status to resolve [#265](https://github.com/smarkets/marge-bot/pull/265)

### `0.9.4`

- Fix: handle `CannotMerge` which could be raised from `update_merge_request` [#275](https://github.com/smarkets/marge-bot/pull/275)
- Fix: maintain `batch_mr_sha` value when batch merging with fast forward commits [#276](https://github.com/smarkets/marge-bot/pull/276)

### `0.9.3`

- Feature: allow merge commits in batch MRs, to make the commits be exactly the same in
  the sub MRs and the batch MR. Add `--use-merge-commit-batches` and `--skip-ci-batches` options [#264](https://github.com/smarkets/marge-bot/pull/264)
- Feature: add `--use-no-ff-batches` to disable fast forwarding of batch merges [#256](https://github.com/smarkets/marge-bot/pull/256) [#259](https://github.com/smarkets/marge-bot/pull/259)

### `0.9.2`

- Fix: ensure parameters are correct when merging with/without pipelines enabled [#251](https://github.com/smarkets/marge-bot/pull/251)
- Fix: only delete source branch if forced [#193](https://github.com/smarkets/marge-bot/pull/193)
- Fix: fix sandboxed build [#250](https://github.com/smarkets/marge-bot/pull/250)

### `0.9.1`

- Feature: support passing a timezone with the embargo [#228](https://github.com/smarkets/marge-bot/pull/228)
- Fix: fix not checking the target project for MRs from forked projects [#218](https://github.com/smarkets/marge-bot/pull/218)

### `0.9.0`

- Feature: support rebasing through GitLab's API [#160](https://github.com/smarkets/marge-bot/pull/160)
- Feature: allow restrict source branches [#206](https://github.com/smarkets/marge-bot/pull/206)
- Fix: only fetch projects with min access level [#166](https://github.com/smarkets/marge-bot/pull/166)
- Fix: bump all dependencies (getting rid of vulnerable packages) [#179](https://github.com/smarkets/marge-bot/pull/179)
- Fix: support multiple assignees [#186](https://github.com/smarkets/marge-bot/pull/186), [#192](https://github.com/smarkets/marge-bot/pull/192)
- Fix: fetch pipelines by merge request instead of branch [#212](https://github.com/smarkets/marge-bot/pull/212)
- Fix: fix unassign when author is Marge [#211](https://github.com/smarkets/marge-bot/pull/211)
- Enhancement: ignore archived projects [#177](https://github.com/smarkets/marge-bot/pull/177)
- Enhancement: add a timeout to all gitlab requests [#200](https://github.com/smarkets/marge-bot/pull/200)
- Enhancement: smaller docker image size  [#199](https://github.com/smarkets/marge-bot/pull/199)

### `0.8.1`

- Feature: allow merging in order of last-update time [#149](https://github.com/smarkets/marge-bot/pull/149)

### `0.8.0`

- Feature: allow reference repository in git clone [#129](https://github.com/smarkets/marge-bot/pull/129)
- Feature: add new stable/master tags for docker images [#142](https://github.com/smarkets/marge-bot/pull/142)
- Fix: fix TypeError when fetching source project [#122](https://github.com/smarkets/marge-bot/pull/122)
- Fix: handle CI status 'skipped' [#127](https://github.com/smarkets/marge-bot/pull/127)
- Fix: handle merging when source branch is master [#127](https://github.com/smarkets/marge-bot/pull/127)
- Fix: handle error on pushing to protected branches [#127](https://github.com/smarkets/marge-bot/pull/127)
- Enhancement: add appropriate error if unresolved discussions on merge request [#136](https://github.com/smarkets/marge-bot/pull/136)
- Enhancement: ensure reviewer and commit author aren't the same [#137](https://github.com/smarkets/marge-bot/pull/137)

### `0.7.0`

- Feature: add `--batch` to better support repos with many daily MRs and slow-ish CI [#84](https://github.com/smarkets/marge-bot/pull/84) [#116](https://github.com/smarkets/marge-bot/pull/116)
- Fix: fix fuse() call when using experimental --use-merge-strategy to update source branch [#102](https://github.com/smarkets/marge-bot/pull/102)
- Fix: Get latest CI status of a commit filtered by branch [#96](https://github.com/smarkets/marge-bot/pull/96) (thanks to benjamb)
- Enhancement: Check MR is mergeable before accepting MR [#117](https://github.com/smarkets/marge-bot/pull/117)

### `0.6.1`

- Fix when target SHA is retrieved [#92](https://github.com/smarkets/marge-bot/pull/92)
- Replace word "gitlab" with "GitLab" [#93](https://github.com/smarkets/marge-bot/pull/93)

### `0.6.0`

- Fix issue due to a `master` branch being assumed when removing
  local branches [#88](https://github.com/smarkets/marge-bot/pull/88)
- Better error reporting when there are no changes left
  after rebasing [#87](https://github.com/smarkets/marge-bot/pull/87)
- Add --approval-reset-timeout option [#85](https://github.com/smarkets/marge-bot/pull/85)
- Fix encoding issues under Windows [#86](https://github.com/smarkets/marge-bot/pull/86)
- Support new merge-request status "locked" [#79](https://github.com/smarkets/marge-bot/pull/79)
- Fixes issue where stale branches in marge's repo could
  lead to conflicts [#78](https://github.com/smarkets/marge-bot/pull/78)
- Add experimental --use-merge-strategy flag that uses merge-commits
  instead of rebasing [#72](https://github.com/smarkets/marge-bot/pull/72) [#90](https://github.com/smarkets/marge-bot/pull/90)

### `0.5.1`

- Sleep even less between polling for MRs [#75](https://github.com/smarkets/marge-bot/pull/75)

### `0.5.0`

- Added "default -> config file -> env var -> args" way to configure marge-bot [#71](https://github.com/smarkets/marge-bot/pull/71)

### `0.4.1`

- Fixed bug in error handling of commit rewritting [#70](https://github.com/smarkets/marge-bot/pull/70) [`1438867`](https://github.com/smarkets/marge-bot/commit/143886720ede2b8a5113e1ee174dbad813ee178b)
- Add --project-regexp argument to restrict to certain target branches [#65](https://github.com/smarkets/marge-bot/pull/65)
- Sleep less between merging requests while there are jobs pending [#67](https://github.com/smarkets/marge-bot/pull/67)
- Less verborragic logging when --debug is used [#66](https://github.com/smarkets/marge-bot/pull/66)

### `0.4.0`

- The official docker image is now on `smarkets/marge-bot` not (`smarketshq/marge-bot`)
- Add a --add-part-of option to tag commit messages with originating MR [#48](https://github.com/smarkets/marge-bot/pull/48)
- Add a --git-timeout parameter (that takes time units); also add --ci-timeout
  that deprecates --max-ci-time-in-minutes [#58](https://github.com/smarkets/marge-bot/pull/58)
- Re-approve immediately after push [#53](https://github.com/smarkets/marge-bot/pull/53)
- Always use --ssh-key-file if passed (never ssh-agent or keys from ~/.ssh) [#61](https://github.com/smarkets/marge-bot/pull/61)
- Fix bad LOCALE problem in official image (hardcode utf-8 everywhere) [#57](https://github.com/smarkets/marge-bot/pull/57)
- Don't blow up on logging bad json responses [#51](https://github.com/smarkets/marge-bot/pull/51)
- Grammar fix [#52](https://github.com/smarkets/marge-bot/pull/52)

### `0.3.2`

Fix support for branches with "/" in their names [#50](https://github.com/smarkets/marge-bot/pull/50)

### `0.3.1`

Fix start-up error when running as non-admin user [#49](https://github.com/smarkets/marge-bot/pull/49)

### `0.3.0`

- Display better messages when GitLab refuses to merge [#32](https://github.com/smarkets/marge-bot/pull/32) [#33](https://github.com/smarkets/marge-bot/pull/33)
- Handle auto-squash being selected [#14](https://github.com/smarkets/marge-bot/pull/14)
- Add `--max-ci-time-in-minutes`, with default of 15 [#44](https://github.com/smarkets/marge-bot/pull/44)
- Fix clean-up of `ssh-key-xxx` files [#38](https://github.com/smarkets/marge-bot/pull/38)
- All command line args now have an environment var equivalent [#35](https://github.com/smarkets/marge-bot/pull/35)

### `0.2.0`

- Add `--project-regexp` flag, to select which projects to include/exclude
- Fix GitLab CE incompatibilities [#30](https://github.com/smarkets/marge-bot/pull/30)

### `0.1.2`

- Fix parsing of GitLab versions [#28](https://github.com/smarkets/marge-bot/pull/28)

### `0.1.1`

- Fix failure to take into account group permissions [#19](https://github.com/smarkets/marge-bot/pull/19)

### `0.1.0`

- Initial release
