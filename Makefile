VERSION?=$$(git rev-parse --abbrev-ref HEAD)
IMAGE_NAME?=registry.gitlab.com/nomadic-labs/marge-bot
DOCKER_REGISTRY?=registry.gitlab.com

check_defined = \
    $(strip $(foreach 1,$1, \
        $(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
    $(if $(value $1),, \
      $(error Undefined $1$(if $2, ($2))))

all: docker

test-ci:
	poetry run pytest

test:
	( docker stop httpbin && docker rm httpbin ) || true
	docker run --rm --name httpbin -d -p 8080:80 kennethreitz/httpbin
	MARGE_RETRY_DELAY=1 MARGE_RETRIES=2 TEST_ENV=docker poetry run pytest
	docker stop httpbin

install:
	poetry install

fmt:
	black marge && black tests

fmt-check:
	black --check marge && black --check tests

docker:
	docker build --tag $(IMAGE_NAME):$$(cat version) .

docker-push:
	if [ -n "$$DOCKER_USERNAME" -a -n "$$DOCKER_PASSWORD" ]; then \
		docker login -u "$${DOCKER_USERNAME}" -p "$${DOCKER_PASSWORD}" $(DOCKER_REGISTRY); \
	else \
		docker login $(DOCKER_REGISTRY); \
	fi
	docker tag $(IMAGE_NAME):$$(cat version) $(IMAGE_NAME):$(VERSION)
	if [ "$(VERSION)" = "$$(cat version)" ]; then \
		docker tag $(IMAGE_NAME):$$(cat version) $(IMAGE_NAME):latest; \
		docker tag $(IMAGE_NAME):$$(cat version) $(IMAGE_NAME):stable; \
		docker push $(IMAGE_NAME):stable; \
		docker push $(IMAGE_NAME):latest; \
	fi
	docker push $(IMAGE_NAME):$(VERSION)
	# for backwards compatibility push to previous location
	docker tag $(IMAGE_NAME):$$(cat version) $(IMAGE_NAME):latest
	docker tag $(IMAGE_NAME):$$(cat version) $(IMAGE_NAME):$(VERSION)
	docker push $(IMAGE_NAME):$(VERSION)
	docker push $(IMAGE_NAME):latest

docker-run:
	docker run --restart=on-failure -e MARGE_AUTH_TOKEN=$(MARGE_GITLAB_TOKEN) registry.gitlab.com/nomadic-labs/marge-bot:$$(cat version) --gitlab-url='https://gitlab.com' --cli --use-https
	
docker-run-full: docker docker-run

sandbox-run:
	$(call check_defined, MARGE_GITLAB_TOKEN)
	@echo auth-token: $$MARGE_GITLAB_TOKEN > .margebot.conf
	@python marge/__main__.py --config-file .margebot.conf --gitlab-url "https://gitlab.com" --cli --use-https                                                           