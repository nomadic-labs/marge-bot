ARG PYTHON_VERSION=3.9.12

FROM python:$PYTHON_VERSION-slim-bullseye AS requirements-exporter

ARG POETRY_VERSION=1.1.13

RUN pip install --no-cache-dir poetry==$POETRY_VERSION

WORKDIR /src

# /!\ Check .dockerignore file
COPY pyproject.toml poetry.lock ./
RUN poetry export -o requirements.txt

FROM requirements-exporter as builder

COPY . .
RUN poetry build

FROM python:$PYTHON_VERSION-slim-bullseye as production

SHELL ["/bin/bash", "-euo", "pipefail", "-c"]

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

RUN addgroup --gid 1000 nomadic \
 && adduser --disabled-password --gecos '' --shell /bin/bash --gid 1000 --uid 1000 nomadic

# hadolint ignore=DL3008
RUN export DEBIAN_FRONTEND=noninteractive \
 && echo 'deb http://deb.debian.org/debian bullseye-backports main' >> /etc/apt/sources.list.d/backports.list \
 && apt-get update \
 && apt-get upgrade -y \
 && apt-get install -y --no-install-recommends -t bullseye-backports git gnupg openssh-client \
 && apt-get autoremove -y \
 && apt-get clean \
 && rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*

RUN python -m pip install --no-cache-dir pip==22.0.4 setuptools==62.1.0 wheel==0.37.1 \
 && pip check

USER nomadic
WORKDIR /home/nomadic
ENV PATH="/home/nomadic/.local/bin:${PATH}"

COPY --chown=nomadic:nomadic --from=requirements-exporter /src/requirements.txt /tmp/
RUN pip install --no-cache-dir -r /tmp/requirements.txt

COPY --chown=nomadic:nomadic --from=builder /src/dist/marge-*.tar.gz /tmp/
RUN pip install --no-cache-dir /tmp/marge-*.tar.gz && rm -rf /tmp/*

ENTRYPOINT ["marge"]
